# include "flow.h"

time_t now = time(0);
tm *ltm = localtime(&now);

pcap_t* open_pcap_socket(char* interface, const char* bpfstr)
{
	pcap_t* pd;
	char errbuf[PCAP_ERRBUF_SIZE];
	uint32_t srcip, netmask;
	struct bpf_program bpf;
	if(!*interface && !(interface = pcap_lookupdev(errbuf)))
	{
		cout<<"\npcap_lookup(): "<<errbuf;
		return NULL;
	}

	if((pd = pcap_open_live(interface, BUFSIZ, 1, 0, errbuf)) == NULL)
	{
		cout<<"\npcap_lookupdev(): "<<errbuf;
		return NULL;
	}

	if(pcap_lookupnet(interface, &srcip, &netmask, errbuf) < 0)
	{
		cout<<"\npcap_lookupnet: "<<errbuf;
		return NULL;
	}

	if(pcap_compile(pd, &bpf, (char*)bpfstr, 0, netmask))
	{
		cout<<"\npcap_compile(): "<<pcap_geterr(pd);
		return NULL;
	}

	if(pcap_setfilter(pd, &bpf) < 0)
	{
		cout<<"\npcap_setfilter(): "<<pcap_geterr(pd);
		return NULL;
	}
	return pd;
}

void bailout(int signo)
{
	struct pcap_stat stats;
	if (pcap_stats(pd, &stats) >= 0)
	{
		cout<<"\nFlow Analysed: "<<stats.ps_recv<<endl;
		cout<<"\nFlow Dropped: "<<stats.ps_drop<<endl;
	}
	pcap_close(pd);
}

void capture_loop(pcap_t* pd, int packets, pcap_handler func)
{
	int linktype;
	if((linktype = pcap_datalink(pd)) < 0)
	{
		cout<<"\npcap_linktype(): "<<pcap_geterr(pd);
		return;
	}
	switch(linktype)
	{
	case DLT_NULL:
		linkhdrlen = 4;
		break;

	case DLT_EN10MB:
		linkhdrlen = 14;
		break;

	case DLT_SLIP:
	case DLT_PPP:
		linkhdrlen = 24;
		break;

	default:
		cout<<"\nUnsupported data-link type: "<<linktype;
		return;
	}
	if(pcap_loop(pd, packets, func, 0) > 0)
		cout<<"\npcap_loop failed: "<<pcap_geterr(pd);
}

void parse_packet(u_char *user, struct pcap_pkthdr *packethdr, u_char *packetptr)
{
	struct ip* iphdr;
	struct icmphdr* icmphdr;
	struct tcphdr* tcphdr;
	struct udphdr* udphdr;

	char srcip[256], dstip[256];
	unsigned short id, seq;

	packetptr += linkhdrlen;
	iphdr = (struct ip*)packetptr;
	strcpy(srcip, inet_ntoa(iphdr -> ip_src));
	strcpy(dstip, inet_ntoa(iphdr -> ip_dst));

	packetptr += 4*iphdr -> ip_hl;
	switch(iphdr -> ip_p)
	{
	case IPPROTO_TCP:
		tcphdr = (struct tcphdr*)packetptr;
		if (tcphdr -> fin)
		{
			if (tcpC == 0)
			{
				if (tcphdr -> ack)
					tcpP[tcpC].flagA = 'A';
				tcpP[tcpC].flagF = 'F';
				strcpy(tcpP[tcpC].srcAddr, srcip);
				strcpy(tcpP[tcpC].dstAddr, dstip);
				tcpP[tcpC].srcP = ntohs(tcphdr -> source);
				tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

				tcpP[tcpC].timeH = ltm -> tm_hour;
				tcpP[tcpC].timeMin = ltm -> tm_min;
				tcpP[tcpC].timeSec = ltm -> tm_sec;
				tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
				tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
				tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
				tcpP[tcpC].npkt = 1;
				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
				cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
						<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" - "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<
						" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagF;
				cout<<"\n-------------------------------------------------------------------------------------------";
				tcpC ++;
			}
			else
			{
				int flag = 0;
				for (int i = 0; i < tcpC; i++)
				{
					if (tcpP[i].flagS == 'S')
					{
						if (strcmp(tcpP[i].srcAddr, dstip) == 0)
						{
							if (strcmp(tcpP[i].dstAddr, srcip) == 0)
							{
								int flag = 1;
								int duration = 0;
								tcpP[i].finalusec = packethdr -> ts.tv_usec;
								duration = tcpP[i].finalusec - tcpP[i].initusec;
								tcpP[i].npkt = tcpP[i].npkt + 1;
								tcpP[i].pktSize = tcpP[i].pktSize + ntohs(iphdr -> ip_len);

								cout<<"\n-------------------------------------------------------------------------------------------";
								cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
								cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec<<" TCP "
										<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<" : "<<tcpP[i].dstP<<
										" "<<tcpP[i].pktSize<<" "<<duration;
								cout<<"\n-------------------------------------------------------------------------------------------";
							}
						}
					}
				}
				if (flag == 0)
				{
					if (tcphdr -> ack)
						tcpP[tcpC].flagA = 'A';
					tcpP[tcpC].flagF = 'F';
					strcpy(tcpP[tcpC].srcAddr,srcip);
					strcpy(tcpP[tcpC].dstAddr,dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
					tcpP[tcpC].npkt = 1;
					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
							<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<
							" "<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagF<<" "<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
					cout<<"\n-------------------------------------------------------------------------------------------";
				}
				tcpC++;
			}
		}

		if (tcphdr -> syn)
		{
			if (tcpC == 0)
			{
				if (tcphdr -> ack)
					tcpP[tcpC].flagA = 'A';
				tcpP[tcpC].flagS = 'S';
				strcpy(tcpP[tcpC].srcAddr,srcip);
				strcpy(tcpP[tcpC].dstAddr,dstip);
				tcpP[tcpC].srcP = ntohs(tcphdr -> source);
				tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

				tcpP[tcpC].timeH = ltm -> tm_hour;
				tcpP[tcpC].timeMin = ltm -> tm_min;
				tcpP[tcpC].timeSec = ltm -> tm_sec;
				tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
				tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
				tcpP[tcpC].npkt = 1;

				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
				cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
						<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
						<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
						<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
				cout<<"\n-------------------------------------------------------------------------------------------";
				tcpC++;
			}
			else
			{
				int flag = 0;
				for(int i = 0; i < tcpC; i++)
				{
					if(tcpP[i].flagF == 'F')
					{
						if(strcmp(tcpP[i].srcAddr, dstip) == 0)
						{
							if(strcmp(tcpP[i].dstAddr, srcip) == 0)
							{
								flag = 1;
								int duration = 0;
								tcpP[i].finalusec = packethdr -> ts.tv_usec;
								duration = tcpP[i].finalusec - tcpP[i].initusec;
								tcpP[i].npkt = tcpP[i].npkt + 1;
								tcpP[i].pktSize = tcpP[i].pktSize + ntohs(iphdr -> ip_len);

								cout<<"\n-------------------------------------------------------------------------------------------";
								cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
								cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec<<" TCP "
										<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<" : "<<tcpP[i].dstP<<" "
										<<tcpP[i].npkt<<" "<<tcpP[i].pktSize<<" "<<tcpP[i].flagS<<tcpP[i].flagA<<" "
										<<tcpP[i].timeSec<<"."<<tcpP[i].initusec;
								cout<<"\n-------------------------------------------------------------------------------------------";

							}
						}
					}
				}
				if (flag == 0)
				{
					if (tcphdr -> ack)
						tcpP[tcpC].flagA = 'A';
					tcpP[tcpC].flagS = 'S';
					strcpy(tcpP[tcpC].srcAddr,srcip);
					strcpy(tcpP[tcpC].dstAddr,dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].npkt = 1;

					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
							<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
							<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
							<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
					cout<<"\n-------------------------------------------------------------------------------------------";
					tcpC++;
				}
			}
		}
		if (tcphdr -> ack)
		{
			if (tcpC == 0)
			{
				tcpP[tcpC].flagA = 'A';
				strcpy(tcpP[tcpC].srcAddr,srcip);
				strcpy(tcpP[tcpC].dstAddr,dstip);
				tcpP[tcpC].srcP = ntohs(tcphdr -> source);
				tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

				tcpP[tcpC].timeH = ltm -> tm_hour;
				tcpP[tcpC].timeMin = ltm -> tm_min;
				tcpP[tcpC].timeSec = ltm -> tm_sec;
				tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
				tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
				tcpP[tcpC].npkt = 1;

				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
				cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
						<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
						<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
						<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
				cout<<"\n-------------------------------------------------------------------------------------------";
				tcpC++;
			}
			else
			{
				int flag = 0;
				int duration = 0;
				for(int i = 0; i < tcpC; i++)
				{
					if (strcmp(srcip, tcpP[i].dstAddr) == 0)
					{
						if (strcmp(srcip, tcpP[i].srcAddr) == 0)
						{
							flag = 1;
							tcpP[i].npkt = tcpP[i].npkt + 1;
							tcpP[i].timeH = ltm -> tm_hour;
							tcpP[i].timeMin = ltm -> tm_min;
							tcpP[i].timeSec = ltm -> tm_sec;
							tcpP[i].finalusec = packethdr -> ts.tv_usec;
							//duration = tcpP[i].initusec - tcpP[i].finalusec;

							cout<<"\n-------------------------------------------------------------------------------------------";
							cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
							cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec
									<<" TCP "<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<":"<<tcpP[i].dstP
									<<" "<<tcpP[i].npkt<<" "<<tcpP[i].pktSize<<" "<<tcpP[i].flagA<<tcpP[i].flagS<<
									tcpP[i].flagF<<" "<<duration;
							cout<<"\n-------------------------------------------------------------------------------------------";
						}
					}
				}

				if (flag == 0)
				{
					tcpP[tcpC].flagA = 'A';

					if (tcphdr -> syn)
						tcpP[tcpC].flagS = 'S';
					strcpy(tcpP[tcpC].srcAddr,srcip);
					strcpy(tcpP[tcpC].dstAddr,dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);
					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
					tcpP[tcpC].npkt = 1;
					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec
							<<" TCP "<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP
							<<" "<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagA<<tcpP[tcpC].flagS<<tcpP[tcpC].flagF
							<<" "<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
					cout<<"\n-------------------------------------------------------------------------------------------";
				}
				tcpC ++;
			}
		}
		break;

	case IPPROTO_UDP:
		udphdr = (struct udphdr*)packetptr;
		if (udpC == 0)
		{
			strcpy(udpP[udpC].srcAddr,srcip);
			strcpy(udpP[udpC].dstAddr,dstip);
			udpP[udpC].srcP = ntohs(udphdr -> source);
			udpP[udpC].dstP = ntohs(udphdr -> dest);
			udpP[udpC].timeH = ltm -> tm_hour;
			udpP[udpC].timeMin = ltm -> tm_min;
			udpP[udpC].timeSec = ltm -> tm_sec;
			udpP[udpC].timeSec = packethdr -> ts.tv_usec;
			udpP[udpC].pktSize = ntohs(iphdr -> ip_len);
			udpP[udpC].hlLength = ntohs(4*iphdr -> ip_hl);
			udpP[udpC].npkt = 1;
			cout<<"\n-------------------------------------------------------------------------------------------";
			cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
			cout<<"\n"<<udpP[udpC].timeH<<":"<<udpP[udpC].timeMin<<":"<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec
					<<" UDP "<<udpP[udpC].srcAddr<<" : "<<udpP[udpC].srcP<<" <-> "<<udpP[udpC].dstAddr<<" : "
					<<udpP[udpC].dstP<<" "<<udpP[udpC].npkt<<" "<<udpP[udpC].pktSize<<" "<<
					udpP[udpC].timeSec<<"."<<udpP[udpC].usec;
			cout<<"\n-------------------------------------------------------------------------------------------";
			udpC = udpC + 1;
		}
		else
		{
			int flag = 0;
			for (int i = 0; i < udpC; i++)
			{
				if (strcmp(srcip,udpP[i].srcAddr) == 0)
				{
					if (strcmp(dstip, udpP[i].dstAddr) == 0)
					{
						flag = 1;
						udpP[i].timeH = ltm -> tm_hour;
						udpP[i].timeMin = ltm -> tm_min;
						udpP[i].timeSec = ltm -> tm_sec;
						udpP[i].timeSec = packethdr -> ts.tv_usec;
						udpP[i].npkt = udpP[i].npkt + 1;
						cout<<"\n-------------------------------------------------------------------------------------------";
						cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
						cout<<"\n"<<udpP[i].timeH<<":"<<udpP[i].timeMin<<":"<<udpP[i].timeSec<<"."<<udpP[i].usec
								<<" UDP "<<udpP[i].srcAddr<<" : "<<udpP[i].srcP<<" <-> "<<udpP[i].dstAddr<<" : "
								<<udpP[i].dstP<<" "<<udpP[i].npkt<<" "<<udpP[i].pktSize<<" "<<udpP[i].timeSec<<"."<<udpP[i].usec;
						cout<<"\n-------------------------------------------------------------------------------------------";
					}
				}
			}

			if (flag == 0)
			{
				strcpy(udpP[udpC].srcAddr,srcip);
				strcpy(udpP[udpC].dstAddr,dstip);
				udpP[udpC].srcP = ntohs(udphdr -> source);
				udpP[udpC].dstP = ntohs(udphdr -> dest);
				udpP[udpC].timeH = ltm -> tm_hour;
				udpP[udpC].timeMin = ltm -> tm_min;
				udpP[udpC].timeSec = ltm -> tm_sec;
				udpP[udpC].timeSec = packethdr -> ts.tv_usec;
				udpP[udpC].pktSize = ntohs(iphdr -> ip_len);
				udpP[udpC].hlLength = ntohs(4*iphdr -> ip_hl);
				udpP[udpC].npkt = 1;
				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
				cout<<"\n"<<udpP[udpC].timeH<<":"<<udpP[udpC].timeMin<<":"<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec
						<<" UDP "<<udpP[udpC].srcAddr<<" : "<<udpP[udpC].srcP<<" <-> "<<udpP[udpC].dstAddr<<" : "
						<<udpP[udpC].dstP<<" "<<udpP[udpC].npkt<<" "<<udpP[udpC].pktSize<<" "<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec;
				cout<<"\n-------------------------------------------------------------------------------------------";
				udpC = udpC + 1;
			}
		}
		break;

	case IPPROTO_ICMP:
		icmphdr = (struct icmphdr*)packetptr;
		if (icmpC == 0)
		{
			strcpy(icmpP[icmpC].srcAddr,srcip);
			strcpy(icmpP[icmpC].dstAddr,dstip);
			icmpP[icmpC].type = icmphdr -> type;
			icmpP[icmpC].timeH = ltm -> tm_hour;
			icmpP[icmpC].timeMin = ltm -> tm_min;
			icmpP[icmpC].timeSec = ltm -> tm_sec;
			icmpP[icmpC].usec = packethdr -> ts.tv_usec;
			icmpP[icmpC].pktSize = ntohs(iphdr -> ip_len);
			icmpP[icmpC].hlLength = ntohs(4*iphdr -> ip_hl);
			icmpP[icmpC].nPkt = 1;
			cout<<"\n-------------------------------------------------------------------------------------------";
			cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
			cout<<"\n"<<icmpP[icmpC].timeH<<":"<<icmpP[icmpC].timeMin<<":"<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec
					<<" ICMP "<<icmpP[icmpC].srcAddr<<" <-> "<<icmpP[icmpC].dstAddr<<" "<<icmpP[icmpC].nPkt<<" "
					<<icmpP[icmpC].pktSize<<" "<<icmpP[icmpC].type<<" "<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec;
			cout<<"\n-------------------------------------------------------------------------------------------";
			icmpC++;
		}
		else
		{
			int flag = 0;
			for (int i = 0; i < icmpC; i++)
			{
				if (strcmp(icmpP[i].srcAddr, srcip) == 0)
				{
					if (strcmp(icmpP[i].dstAddr, dstip) == 0)
					{
						flag = 1;
						icmpP[i].nPkt = icmpP[i].nPkt + 1;
						cout<<"\n-------------------------------------------------------------------------------------------";
						cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
						cout<<"\n"<<icmpP[i].timeH<<":"<<icmpP[i].timeMin<<":"<<icmpP[i].timeSec<<"."<<icmpP[i].usec
								<<" ICMP "<<icmpP[i].srcAddr<<" <-> "<<icmpP[i].dstAddr<<" "<<icmpP[i].nPkt<<" "
								<<icmpP[i].pktSize<<" "<<icmpP[i].type<<" "<<icmpP[i].timeSec<<"."<<icmpP[i].usec;
						cout<<"\n-------------------------------------------------------------------------------------------";
					}
				}
			}

			if (flag == 0)
			{
				strcpy(icmpP[icmpC].srcAddr,srcip);
				strcpy(icmpP[icmpC].dstAddr,dstip);
				icmpP[icmpC].type = icmphdr -> type;
				icmpP[icmpC].timeH = ltm -> tm_hour;
				icmpP[icmpC].timeMin = ltm -> tm_min;
				icmpP[icmpC].timeSec = ltm -> tm_sec;
				icmpP[icmpC].usec = packethdr -> ts.tv_usec;
				icmpP[icmpC].pktSize = ntohs(iphdr -> ip_len);
				icmpP[icmpC].hlLength = ntohs(4*iphdr -> ip_hl);
				icmpP[icmpC].nPkt = 1;
				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
				cout<<"\n"<<icmpP[icmpC].timeH<<":"<<icmpP[icmpC].timeMin<<":"<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec
						<<" ICMP "<<icmpP[icmpC].srcAddr<<" <-> "<<icmpP[icmpC].dstAddr<<" "<<icmpP[icmpC].nPkt<<" "
						<<icmpP[icmpC].pktSize<<" "<<icmpP[icmpC].type<<" "<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec;
				cout<<"\n-------------------------------------------------------------------------------------------";
			}
			icmpC++;
		}
		break;
	default:
		cout<<"";
		break;
	}
}

void offlineAnalysis(int, int, int, int, char*);
int main(int argc, char **argv)
{
	system("clear");
	int nPackets = 0;
	while((opt = getopt(argc, argv, "r:i:t:o:N:S:")) != -1)
	{
		switch(opt)
		{
		case 'i':
			strcpy(interface, optarg);
			break;
		case 'r':
			file = optarg;
			break;
		case 't':
			t = atoi(optarg);
			break;
		case 'o':
			offSet = atoi(optarg);
			break;
		case 'N':
			nFlows = atoi(optarg);
			break;
		case 'S':
			secTimeout = atoi(optarg);
			break;
		default:
			cout<<"\nUnknown parameters";
			abort();
		}
	}

	if (file != '\0')
	{
		offlineAnalysis(t, offSet, secTimeout, nFlows, file);
	}
	else
	{
		cout<<"\n------------------------------------";
		cout<<"\nAnalyzing live flows..!!";
		cout<<"\nParameters";
		cout<<"\n------------------------------------";
		cout<<"\nInterface: "<<interface;
		cout<<"\nTime: "<<t;
		cout<<"\nOff-Set: "<<offSet;
		cout<<"\n# of Flows: "<<nFlows;
		cout<<"\n# of Seconds: "<<secTimeout;
		cout<<"\n-------------------------------------"<<endl;
		int i;
		usleep(10*100*100);
		for (i = optind; i < argc; i++)
		{
			strcpy(bpfstr, argv[i]);
			strcpy(bpfstr, " ");
		}

		if((pd = open_pcap_socket(interface, bpfstr)))
		{
			signal(SIGINT, bailout);
			signal(SIGTERM, bailout);
			signal(SIGTERM, bailout);
			capture_loop(pd, nPackets, (pcap_handler)parse_packet);
			bailout(0);
		}
		exit(0);
	}
	return 0;
}

void offlineAnalysis(int t, int offSet, int secTimeout, int nFlows, char* file)
{
	cout<<"\n------------------------------------";
	cout<<"\nAnalyzing flow from file..!!";
	cout<<"\nParameters";
	cout<<"\n------------------------------------";
	cout<<"\nFile: "<<file;
	cout<<"\nTime: "<<t;
	cout<<"\nOff-Set: "<<offSet;
	cout<<"\n# of Flows: "<<nFlows;
	cout<<"\n# of Seconds: "<<secTimeout;
	cout<<"\n-------------------------------------"<<endl;
	char errbuff[PCAP_ERRBUF_SIZE];
	pcap_t* user = pcap_open_offline(file, errbuff);
	struct pcap_pkthdr *packethdr;
	const u_char *packetptr;

	while(pcap_next_ex(user, &packethdr, &packetptr) > 0)
	{
		ostringstream iphdrInfo;
		char srcip[256], dstip[256];
		//unsigned short id, seq;

		struct ip* iphdr;
		struct icmphdr* icmphdr;
		struct tcphdr* tcphdr;
		struct udphdr* udphdr;

		// Skipping the datalink layer header and get the IP header fields.
		packetptr += linkhdrlen;
		iphdr = (struct ip*)packetptr;
		strcpy(srcip, inet_ntoa(iphdr -> ip_src));
		strcpy(dstip, inet_ntoa(iphdr -> ip_dst));
		iphdrInfo<< "ID: "<<ntohs(iphdr->ip_id)<<" IpLen: "<<
				4*iphdr->ip_hl<<" DgLen: "<<ntohs(iphdr->ip_len);

		// Advance to the transport layer header then parse and display
		// the fields based on the type of hearder: tcp, udp or icmp.
		packetptr += 4*iphdr->ip_hl;

		switch (iphdr -> ip_p)
		{
		case IPPROTO_TCP:
			if (tcphdr -> fin)
			{
				if (tcpC == 0)
				{
					if (tcphdr -> ack)
						tcpP[tcpC].flagA = 'A';
					tcpP[tcpC].flagF = 'F';
					strcpy(tcpP[tcpC].srcAddr, srcip);
					strcpy(tcpP[tcpC].dstAddr, dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
					tcpP[tcpC].npkt = 1;
					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
							<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" - "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<
							" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagF;
					cout<<"\n-------------------------------------------------------------------------------------------";
					tcpC ++;
				}
				else
				{
					int flag = 0;
					for (int i = 0; i < tcpC; i++)
					{
						if (tcpP[i].flagS == 'S')
						{
							if (strcmp(tcpP[i].srcAddr, dstip) == 0)
							{
								if (strcmp(tcpP[i].dstAddr, srcip) == 0)
								{
									int flag = 1;
									int duration = 0;
									tcpP[i].finalusec = packethdr -> ts.tv_usec;
									duration = tcpP[i].finalusec - tcpP[i].initusec;
									tcpP[i].npkt = tcpP[i].npkt + 1;
									tcpP[i].pktSize = tcpP[i].pktSize + ntohs(iphdr -> ip_len);

									cout<<"\n-------------------------------------------------------------------------------------------";
									cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
									cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec<<" TCP "
											<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<" : "<<tcpP[i].dstP<<
											" "<<tcpP[i].pktSize<<" "<<duration;
									cout<<"\n-------------------------------------------------------------------------------------------";
								}
							}
						}
					}
					if (flag == 0)
					{
						if (tcphdr -> ack)
							tcpP[tcpC].flagA = 'A';
						tcpP[tcpC].flagF = 'F';
						strcpy(tcpP[tcpC].srcAddr,srcip);
						strcpy(tcpP[tcpC].dstAddr,dstip);
						tcpP[tcpC].srcP = ntohs(tcphdr -> source);
						tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

						tcpP[tcpC].timeH = ltm -> tm_hour;
						tcpP[tcpC].timeMin = ltm -> tm_min;
						tcpP[tcpC].timeSec = ltm -> tm_sec;
						tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
						tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
						tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
						tcpP[tcpC].npkt = 1;
						cout<<"\n-------------------------------------------------------------------------------------------";
						cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
						cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
								<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<
								" "<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagF<<" "<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
						cout<<"\n-------------------------------------------------------------------------------------------";
					}
					tcpC++;
				}
			}

			if (tcphdr -> syn)
			{
				if (tcpC == 0)
				{
					if (tcphdr -> ack)
						tcpP[tcpC].flagA = 'A';
					tcpP[tcpC].flagS = 'S';
					strcpy(tcpP[tcpC].srcAddr,srcip);
					strcpy(tcpP[tcpC].dstAddr,dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].npkt = 1;

					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
							<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
							<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
							<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
					cout<<"\n-------------------------------------------------------------------------------------------";
					tcpC++;
				}
				else
				{
					int flag = 0;
					for(int i = 0; i < tcpC; i++)
					{
						if(tcpP[i].flagF == 'F')
						{
							if(strcmp(tcpP[i].srcAddr, dstip) == 0)
							{
								if(strcmp(tcpP[i].dstAddr, srcip) == 0)
								{
									flag = 1;
									int duration = 0;
									tcpP[i].finalusec = packethdr -> ts.tv_usec;
									duration = tcpP[i].finalusec - tcpP[i].initusec;
									tcpP[i].npkt = tcpP[i].npkt + 1;
									tcpP[i].pktSize = tcpP[i].pktSize + ntohs(iphdr -> ip_len);

									cout<<"\n-------------------------------------------------------------------------------------------";
									cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
									cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec<<" TCP "
											<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<" : "<<tcpP[i].dstP<<" "
											<<tcpP[i].npkt<<" "<<tcpP[i].pktSize<<" "<<tcpP[i].flagS<<tcpP[i].flagA<<" "
											<<tcpP[i].timeSec<<"."<<tcpP[i].initusec;
									cout<<"\n-------------------------------------------------------------------------------------------";

								}
							}
						}
					}
					if (flag == 0)
					{
						if (tcphdr -> ack)
							tcpP[tcpC].flagA = 'A';
						tcpP[tcpC].flagS = 'S';
						strcpy(tcpP[tcpC].srcAddr,srcip);
						strcpy(tcpP[tcpC].dstAddr,dstip);
						tcpP[tcpC].srcP = ntohs(tcphdr -> source);
						tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

						tcpP[tcpC].timeH = ltm -> tm_hour;
						tcpP[tcpC].timeMin = ltm -> tm_min;
						tcpP[tcpC].timeSec = ltm -> tm_sec;
						tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
						tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
						tcpP[tcpC].npkt = 1;

						cout<<"\n-------------------------------------------------------------------------------------------";
						cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
						cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
								<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
								<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
								<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
						cout<<"\n-------------------------------------------------------------------------------------------";
						tcpC++;
					}
				}
			}

			if (tcphdr -> ack)
			{
				if (tcpC == 0)
				{
					tcpP[tcpC].flagA = 'A';
					strcpy(tcpP[tcpC].srcAddr,srcip);
					strcpy(tcpP[tcpC].dstAddr,dstip);
					tcpP[tcpC].srcP = ntohs(tcphdr -> source);
					tcpP[tcpC].dstP = ntohs(tcphdr -> dest);

					tcpP[tcpC].timeH = ltm -> tm_hour;
					tcpP[tcpC].timeMin = ltm -> tm_min;
					tcpP[tcpC].timeSec = ltm -> tm_sec;
					tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
					tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
					tcpP[tcpC].npkt = 1;

					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec<<" TCP "
							<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP<<" "
							<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagS<<tcpP[tcpC].flagA<<" "
							<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
					cout<<"\n-------------------------------------------------------------------------------------------";
					tcpC++;
				}
				else
				{
					int flag = 0;
					int duration = 0;
					for(int i = 0; i < tcpC; i++)
					{
						if (strcmp(srcip, tcpP[i].dstAddr) == 0)
						{
							if (strcmp(srcip, tcpP[i].srcAddr) == 0)
							{
								flag = 1;
								tcpP[i].npkt = tcpP[i].npkt + 1;
								tcpP[i].timeH = ltm -> tm_hour;
								tcpP[i].timeMin = ltm -> tm_min;
								tcpP[i].timeSec = ltm -> tm_sec;
								tcpP[i].finalusec = packethdr -> ts.tv_usec;
								//duration = tcpP[i].initusec - tcpP[i].finalusec;

								cout<<"\n-------------------------------------------------------------------------------------------";
								cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
								cout<<"\n"<<tcpP[i].timeH<<":"<<tcpP[i].timeMin<<":"<<tcpP[i].timeSec<<"."<<tcpP[i].initusec
										<<" TCP "<<tcpP[i].srcAddr<<" : "<<tcpP[i].srcP<<" <-> "<<tcpP[i].dstAddr<<":"<<tcpP[i].dstP
										<<" "<<tcpP[i].npkt<<" "<<tcpP[i].pktSize<<" "<<tcpP[i].flagA<<tcpP[i].flagS<<
										tcpP[i].flagF<<" "<<duration;
								cout<<"\n-------------------------------------------------------------------------------------------";
							}
						}
					}

					if (flag == 0)
					{
						tcpP[tcpC].flagA = 'A';

						if (tcphdr -> syn)
							tcpP[tcpC].flagS = 'S';
						strcpy(tcpP[tcpC].srcAddr,srcip);
						strcpy(tcpP[tcpC].dstAddr,dstip);
						tcpP[tcpC].srcP = ntohs(tcphdr -> source);
						tcpP[tcpC].dstP = ntohs(tcphdr -> dest);
						tcpP[tcpC].timeH = ltm -> tm_hour;
						tcpP[tcpC].timeMin = ltm -> tm_min;
						tcpP[tcpC].timeSec = ltm -> tm_sec;
						tcpP[tcpC].initusec = packethdr -> ts.tv_usec;
						tcpP[tcpC].pktSize = ntohs(iphdr -> ip_len);
						tcpP[tcpC].hlLength = ntohs(4*iphdr -> ip_hl);
						tcpP[tcpC].npkt = 1;
						cout<<"\n-------------------------------------------------------------------------------------------";
						cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	State	Dur";
						cout<<"\n"<<tcpP[tcpC].timeH<<":"<<tcpP[tcpC].timeMin<<":"<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec
								<<" TCP "<<tcpP[tcpC].srcAddr<<" : "<<tcpP[tcpC].srcP<<" <-> "<<tcpP[tcpC].dstAddr<<" : "<<tcpP[tcpC].dstP
								<<" "<<tcpP[tcpC].npkt<<" "<<tcpP[tcpC].pktSize<<" "<<tcpP[tcpC].flagA<<tcpP[tcpC].flagS<<tcpP[tcpC].flagF
								<<" "<<tcpP[tcpC].timeSec<<"."<<tcpP[tcpC].initusec;
						cout<<"\n-------------------------------------------------------------------------------------------";
					}
					tcpC ++;
				}
			}
			break;

		case IPPROTO_UDP:
			udphdr = (struct udphdr*)packetptr;
			if (udpC == 0)
			{
				strcpy(udpP[udpC].srcAddr,srcip);
				strcpy(udpP[udpC].dstAddr,dstip);
				udpP[udpC].srcP = ntohs(udphdr -> source);
				udpP[udpC].dstP = ntohs(udphdr -> dest);
				udpP[udpC].timeH = ltm -> tm_hour;
				udpP[udpC].timeMin = ltm -> tm_min;
				udpP[udpC].timeSec = ltm -> tm_sec;
				udpP[udpC].timeSec = packethdr -> ts.tv_usec;
				udpP[udpC].pktSize = ntohs(iphdr -> ip_len);
				udpP[udpC].hlLength = ntohs(4*iphdr -> ip_hl);
				udpP[udpC].npkt = 1;
				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
				cout<<"\n"<<udpP[udpC].timeH<<":"<<udpP[udpC].timeMin<<":"<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec
						<<" UDP "<<udpP[udpC].srcAddr<<" : "<<udpP[udpC].srcP<<" <-> "<<udpP[udpC].dstAddr<<" : "
						<<udpP[udpC].dstP<<" "<<udpP[udpC].npkt<<" "<<udpP[udpC].pktSize<<" "<<
						udpP[udpC].timeSec<<"."<<udpP[udpC].usec;
				cout<<"\n-------------------------------------------------------------------------------------------";
				udpC = udpC + 1;
			}
			else
			{
				int flag = 0;
				for (int i = 0; i < udpC; i++)
				{
					if (strcmp(srcip,udpP[i].srcAddr) == 0)
					{
						if (strcmp(dstip, udpP[i].dstAddr) == 0)
						{
							flag = 1;
							udpP[i].timeH = ltm -> tm_hour;
							udpP[i].timeMin = ltm -> tm_min;
							udpP[i].timeSec = ltm -> tm_sec;
							udpP[i].timeSec = packethdr -> ts.tv_usec;
							udpP[i].npkt = udpP[i].npkt + 1;
							cout<<"\n-------------------------------------------------------------------------------------------";
							cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
							cout<<"\n"<<udpP[i].timeH<<":"<<udpP[i].timeMin<<":"<<udpP[i].timeSec<<"."<<udpP[i].usec
									<<" UDP "<<udpP[i].srcAddr<<" : "<<udpP[i].srcP<<" <-> "<<udpP[i].dstAddr<<" : "
									<<udpP[i].dstP<<" "<<udpP[i].npkt<<" "<<udpP[i].pktSize<<" "<<udpP[i].timeSec<<"."<<udpP[i].usec;
							cout<<"\n-------------------------------------------------------------------------------------------";
						}
					}
				}

				if (flag == 0)
				{
					strcpy(udpP[udpC].srcAddr,srcip);
					strcpy(udpP[udpC].dstAddr,dstip);
					udpP[udpC].srcP = ntohs(udphdr -> source);
					udpP[udpC].dstP = ntohs(udphdr -> dest);
					udpP[udpC].timeH = ltm -> tm_hour;
					udpP[udpC].timeMin = ltm -> tm_min;
					udpP[udpC].timeSec = ltm -> tm_sec;
					udpP[udpC].timeSec = packethdr -> ts.tv_usec;
					udpP[udpC].pktSize = ntohs(iphdr -> ip_len);
					udpP[udpC].hlLength = ntohs(4*iphdr -> ip_hl);
					udpP[udpC].npkt = 1;
					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	SrcPort Dir DestIP	DestP	TotPkt	TotSize	Dur";
					cout<<"\n"<<udpP[udpC].timeH<<":"<<udpP[udpC].timeMin<<":"<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec
							<<" UDP "<<udpP[udpC].srcAddr<<" : "<<udpP[udpC].srcP<<" <-> "<<udpP[udpC].dstAddr<<" : "
							<<udpP[udpC].dstP<<" "<<udpP[udpC].npkt<<" "<<udpP[udpC].pktSize<<" "<<udpP[udpC].timeSec<<"."<<udpP[udpC].usec;
					cout<<"\n-------------------------------------------------------------------------------------------";
					udpC = udpC + 1;
				}
			}
			break;

		case IPPROTO_ICMP:
			icmphdr = (struct icmphdr*)packetptr;
			if (icmpC == 0)
			{
				strcpy(icmpP[icmpC].srcAddr,srcip);
				strcpy(icmpP[icmpC].dstAddr,dstip);
				icmpP[icmpC].type = icmphdr -> type;
				icmpP[icmpC].timeH = ltm -> tm_hour;
				icmpP[icmpC].timeMin = ltm -> tm_min;
				icmpP[icmpC].timeSec = ltm -> tm_sec;
				icmpP[icmpC].usec = packethdr -> ts.tv_usec;
				icmpP[icmpC].pktSize = ntohs(iphdr -> ip_len);
				icmpP[icmpC].hlLength = ntohs(4*iphdr -> ip_hl);
				icmpP[icmpC].nPkt = 1;
				cout<<"\n-------------------------------------------------------------------------------------------";
				cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
				cout<<"\n"<<icmpP[icmpC].timeH<<":"<<icmpP[icmpC].timeMin<<":"<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec
						<<" ICMP "<<icmpP[icmpC].srcAddr<<" <-> "<<icmpP[icmpC].dstAddr<<" "<<icmpP[icmpC].nPkt<<" "
						<<icmpP[icmpC].pktSize<<" "<<icmpP[icmpC].type<<" "<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec;
				cout<<"\n-------------------------------------------------------------------------------------------";
				icmpC++;
			}
			else
			{
				int flag = 0;
				for (int i = 0; i < icmpC; i++)
				{
					if (strcmp(icmpP[i].srcAddr, srcip) == 0)
					{
						if (strcmp(icmpP[i].dstAddr, dstip) == 0)
						{
							flag = 1;
							icmpP[i].nPkt = icmpP[i].nPkt + 1;
							cout<<"\n-------------------------------------------------------------------------------------------";
							cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
							cout<<"\n"<<icmpP[i].timeH<<":"<<icmpP[i].timeMin<<":"<<icmpP[i].timeSec<<"."<<icmpP[i].usec
									<<" ICMP "<<icmpP[i].srcAddr<<" <-> "<<icmpP[i].dstAddr<<" "<<icmpP[i].nPkt<<" "
									<<icmpP[i].pktSize<<" "<<icmpP[i].type<<" "<<icmpP[i].timeSec<<"."<<icmpP[i].usec;
							cout<<"\n-------------------------------------------------------------------------------------------";
						}
					}
				}

				if (flag == 0)
				{
					strcpy(icmpP[icmpC].srcAddr,srcip);
					strcpy(icmpP[icmpC].dstAddr,dstip);
					icmpP[icmpC].type = icmphdr -> type;
					icmpP[icmpC].timeH = ltm -> tm_hour;
					icmpP[icmpC].timeMin = ltm -> tm_min;
					icmpP[icmpC].timeSec = ltm -> tm_sec;
					icmpP[icmpC].usec = packethdr -> ts.tv_usec;
					icmpP[icmpC].pktSize = ntohs(iphdr -> ip_len);
					icmpP[icmpC].hlLength = ntohs(4*iphdr -> ip_hl);
					icmpP[icmpC].nPkt = 1;
					cout<<"\n-------------------------------------------------------------------------------------------";
					cout<<"\nTimestamp	Proto	SrcIP	Dir	DestIP	TotPkt	TotSize	State	Dur";
					cout<<"\n"<<icmpP[icmpC].timeH<<":"<<icmpP[icmpC].timeMin<<":"<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec
							<<" ICMP "<<icmpP[icmpC].srcAddr<<" <-> "<<icmpP[icmpC].dstAddr<<" "<<icmpP[icmpC].nPkt<<" "
							<<icmpP[icmpC].pktSize<<" "<<icmpP[icmpC].type<<" "<<icmpP[icmpC].timeSec<<"."<<icmpP[icmpC].usec;
					cout<<"\n-------------------------------------------------------------------------------------------";
				}
				icmpC++;
			}
			break;
		default:
			cout<<"";
			break;
		}
	}
}
