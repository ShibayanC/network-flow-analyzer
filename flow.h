# include <iostream>
# include <stdlib.h>
# include <pcap.h>
# include <errno.h>
# include <getopt.h>
# include <string>
# include <signal.h>
# include <cstring>
# include <unistd.h>
# include <sstream>
# include <stdio.h>
# include <ctime>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netinet/ip.h>
# include <netinet/tcp.h>
# include <netinet/udp.h>
# include <netinet/ip_icmp.h>

using namespace std;

# define SIZE_ETHERNET 14
# define ETHER_ADDR_LEN 6

pcap_t* pd;
int linkhdrlen;

char *tcpPtr[100];  // Array of objects for tcp
int *udpPtr[100];  // Array of objects for udp
int *icmpPtr[100];  // Array of objects for icmp

int nFlows = 0, secTimeout = 0;  // Nos of flows; Time out for each flow
char interface[256] = "";
char bpfstr[256] = "";
int t = 0;
int offSet = 0;
char *file = '\0';
int opt;

int tcpC = 0, udpC = 0, icmpC = 0;

struct tcppacket
{
	char srcAddr[256];
	char dstAddr[256];
	long int srcP;
	long int dstP;

	char flagS;
	char flagA;
	char flagF;

	int timeH;
	int timeMin;
	int timeSec;
	double initusec;
	double finalusec;

	int pktSize;
	int hlLength;
	int npkt;
};

struct tcppacket tcpP[100];

struct udppacket
{
	char srcAddr[256];
	char dstAddr[256];
	long int srcP;
	long int dstP;
	double usec;
	int timeH;
	int timeMin;
	int timeSec;
	int npkt;
	int pktSize;
	int hlLength;
};

struct udppacket udpP[100];

struct icmppacket
{
	char srcAddr[256];
	char dstAddr[256];
	char type;
	char code;
	char id;
	double usec;
	int timeH;
	int timeMin;
	int timeSec;
	int nPkt;
	int pktSize;
	int hlLength;
};

struct icmppacket icmpP[100];
