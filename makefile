OBJS = flow.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAG = -Wall $(DEBUG)

h1 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o flow -lpcap

flow.o: flow.h flow.cpp
	$(CC) $(CFLAGS) flow.cpp

clean:
	-rm -f h2 *.o 

tar:
	tar cfv h2.shibayan.tar flow.h flow.cpp makefile README
